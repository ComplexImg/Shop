<?php

//ini_set('display_errors', 1);

spl_autoload_register(function($path){
    $path = __DIR__ . '\\'. $path . '.php';

    $path = str_replace("\\", "/", $path);

    $temp = substr($path, 0, strrpos($path, "/"));

    $temp2 = substr($path, strrpos($path, "/") + 2, strlen($path));



    $path = $temp. '/'.strtolower($path[strrpos($path, "/") + 1]) . $temp2;
 //   echo $path . "<br>";

    if (file_exists($path))
        require_once $path;
});

$GLOBALS['path'] = '/application';

// /application/kendo/lib/Kendo/Autoload.php
// /var/www/u0349149/data/www/velokosmos.ru/application/kendo/lib/Kendo/Autoload.php

application\core\Main::goWork();
