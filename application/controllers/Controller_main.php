<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;

class Controller_main extends \application\core\Controller
{

    function __construct()
    {
        $this->modelAjax = new \application\models\modelAjax();
        $this->view = new \application\core\View();
        $this->model = new \application\models\modelMain();
    }


    function action_index()
    {
        try {
            $menuArray = $this->model->getMainArray();

            $this->view->generate('main/main.phtml', 'Template.phtml', array(
                'title' => 'Велокосмос - Добро пожаловать',
                'menu' => $this->model->getMainMenu($menuArray),
                'news' => $this->model->getMainData('news', 4),
                'top_products' => $this->model->getTopProducts(4),
                'articles' => $this->model->getMainData('articles', 4),
                'act' => $this->model->getRandActProducts(4),
                'slider' => $this->model->getSlider(),
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_page($id)
    {
        try {

            $id = explode('/', $id);

            if ($id[0] === null)
                throw new Exception404();

            $this->model->db->connect();


            $data = $this->model->db->selectSiteContent(array('site_content', $id[0]));

            if ($data == null)
                throw new Exception404();

            $menuArray = $this->model->getMainArray();

            switch ($id[0]) {
                case 'guarantee':
                    $page = 'Гарантия';
                    break;
                case 'payment':
                    $page = 'Оплата';
                    break;
                case 'delivery':
                    $page = 'Доставка';
                    break;
                case 'contact':
                    $page = 'Контакты';
                    break;
                case 'about':
                    $page = 'О нас';
                    break;
                default:
                    throw new Exception404();
            }

            $this->view->generate('main/site_content.phtml', 'Template.phtml', array(
                'menu' => $this->model->getMainMenu($menuArray),
                'content' => $data,
                'page' => $page,
                'title' => 'Велокосмос - ' . $page,
                'slider' => $this->model->getSlider(),
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_menu($id)
    {
        try {

            $id = explode('/', $id);

            $data['menuArray'] = $this->model->getMainArray();

            $data['name'] = urldecode($id[0]);

            if (! array_key_exists($data['name'], $data['menuArray']))
                throw new Exception404();

            $this->model->db->connect();

            $category = $this->model->db->selectTable(array('category'));


            foreach ($category as $cat) {
                if (in_array($cat['c_key'],  $data['menuArray'][$data['name']]))
                    $data['content'][] = $cat;
            }

            $this->view->generate('main/menu.phtml', 'Template.phtml', array(
                'name' => $data['name'],
                'content' => $data['content'],
                'menu_array' => $data['menuArray'],
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


}
