<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;

class controller_products extends \application\core\Controller
{

    function __construct()
    {
        $this->modelAjax = new \application\models\modelAjax();
        $this->model = new \application\models\modelProducts();
        $this->view = new \application\core\View();
    }


    function action_index($id)
    {
        throw new Exception404();
    }


    function action_select($id)
    {
        try {
            $id = explode('/', $id);

            if ($id[0] == null)
                throw new Exception404;

            $data = $this->model->getProduct($id);

            session_start();
            $products = json_decode($_SESSION['products'], true);
            $productsArticles = json_decode($_SESSION['articles'], true);

            if ($products != null) {
                $cart = $this->model->getShoppinCart($products);
                $count = $cart['content'][$id[0]]['count'];
                if ($count == null)
                    $count = 0;

            } else {
                $count = 0;
            }

            $data['product_img'] = $this->model->db->readWhere('product_img', 'id_product', $id[0]);
            $data['manufactorer'] = $this->model->db->selectTable(array('manufactorer'));
            $data['manufactorer'] = $this->model->idKeys($data['manufactorer'], 'id');

            $this->view->generate('products/select.phtml', 'Template_oneFloatSlider.phtml', array(
                'content' => $data['content'],
                'oldFilters' => $data['oldFilters'],
                'filter_type' => $data['filter_type'],
                'catFilters' => $data['catFilters'],
                'category' => $data['category'],
                'count' => $count,
                'articles' => $productsArticles,
                'manufactorer' => $data['manufactorer'],
                'product_img' => $data['product_img'],
                'otz' => $data['otz'],
                'title' => 'Велокосмос - ' . $data['content']['name'],
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_shopping_cart()
    {
        try {

            session_start();
            $productsArticles = json_decode($_SESSION['articles'], true);

            if ($_POST != null) {
                $this->model->updateShoppingCart($_POST, $productsArticles);
                $productsArticles = json_decode($_SESSION['articles'], true);
            }

            $data = array();

            if ($productsArticles != null) {
                $data = $this->model->getShoppinCart($productsArticles);
            }

            $this->view->generate('products/shopping_cart.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'title' => 'Велокосмос - Корзина товаров',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_new_order()
    {
        try {

            session_start();
            $products = json_decode($_SESSION['products'], true);
            $productsArticles = json_decode($_SESSION['articles'], true);
            $data = array();

            if ($products != null) {
                $data = $this->model->getShoppinCart($productsArticles);
                if ($_POST != null) {

                    $resData = $this->model->dataFormat->titleFormat($_POST);
                    $number = $this->model->db->getOrderId();
                    $number = $number['id'] + 1;

                    $email = new \application\data\emailSender($number, $resData, $data);

                    $sendUser = $email->sendEmailForUser();

                    if($sendUser){
                        $sendAdmin = $email->sendEmailForAdmin();
                        if ($sendAdmin)
                            $result = $this->model->newOrder($data, $resData);
                        else
                            $result = false;
                    }else
                        $result = false;

                }

                $obl = $this->model->db->selectTable(array('obl_prices'));
                $sam = $this->model->db->selectTable(array('samovivoz'));

            }


            //$data['delivery'] = $this->model->db->selectSiteContent(array('site_content', 'delivery'));

            $this->view->generate('products/new_order.phtml', 'Template.phtml', array(
                'number' => $number,
                'content' => $data['content'],
                'obl' => $obl,
                'sam' => $sam,
                'title' => 'Велокосмос - Оформление заказа',
                'flag' => $result,
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_list($id)
    {
        try {
            $id = explode('/', $id);

            if ($id[0] == null)
                throw new Exception404;

            $data = $this->model->productList($id, $_POST);

            $this->view->generate('products/list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'prices' => $data['price'],
                'cat_filter_type' => $data['cat_filter_type'],
                'filter_type' => $data['filter_type'],
                'cat_filters' => $data['cat_filters'],
                'category' => $data['category'],
                'pr_filters' => $data['pr_filters'],
                'manufactorer' => $data['manufactorer'],
                'pages' => $data['pages'],
                'post' => $data['post'],
                'title' => 'Велокосмос - Список товаров - ' . $data['category']['name'],
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));


        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_act($id)
    {
        try {
            $this->model->db->connect();

            $val = $this->model->db->getSettingsCount();
            $val = (int)$val['products_count'];

            $page = (int)$id[0];
            if ($page === 0 and $id[0] != '')
                throw new Exception404();

            if ($id[0] == null || $id[0] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[0]) - $val;
            }

            $count = $this->model->db->selectActProductsCount();
            $count = (int)$count['COUNT(*)'];

            $data['pages']['count'] = $count;
            $data['pages']['select'] = ($page == 0) ? 1 : $page;
            $data['pages']['size'] = $val;
            $data['pages']['page'] = '/products/act/';

            $data['filter_type'] = $this->model->db->selectTable(array("filter_type"));

            $data['content'] = $this->model->db->selectActProducts($with, $val);

            $data['manufactorer'] = $this->model->db->selectTable(array('manufactorer'));
            $data['manufactorer'] = $this->model->idKeys($data['manufactorer'], 'id');

            foreach ($data['content'] as $product) {
                $data['pr_filters'][$product['id']] = $this->model->productGetFilters($product);
            }

            $this->view->generate('products/act_list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'prices' => $data['price'],
                'cat_filter_type' => $data['cat_filter_type'],
                'filter_type' => $data['filter_type'],
                'cat_filters' => $data['cat_filters'],
                'category' => $data['category'],
                'pr_filters' => $data['pr_filters'],
                'manufactorer' => $data['manufactorer'],
                'pages' => $data['pages'],
                'path' => $data['path'],
                'post' => $data['post'],
                'topPath' => 'act',
                'topName' => 'Акции',
                'title' => 'Велокосмос - Аукционные товары',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));


        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_top($id)
    {
        try {
            $this->model->db->connect();

            $val = $this->model->db->getSettingsCount();
            $val = (int)$val['products_count'];

            $page = (int)$id[0];
            if ($page === 0 and $id[0] != '')
                throw new Exception404();

            if ($id[0] == null || $id[0] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[0]) - $val;
            }

            $count = $this->model->db->selectTopProductsCount();
            $count = (int)$count['COUNT(*)'];

            $data['pages']['count'] = $count;
            $data['pages']['select'] = ($page == 0) ? 1 : $page;
            $data['pages']['size'] = $val;
            $data['pages']['page'] = '/products/top/';

            $data['filter_type'] = $this->model->db->selectTable(array("filter_type"));

            $data['content'] = $this->model->db->selectTopProducts($with, $val);

            $data['manufactorer'] = $this->model->db->selectTable(array('manufactorer'));
            $data['manufactorer'] = $this->model->idKeys($data['manufactorer'], 'id');

            foreach ($data['content'] as $product) {
                $data['pr_filters'][$product['id']] = $this->model->productGetFilters($product);
            }

            $this->view->generate('products/act_list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'prices' => $data['price'],
                'cat_filter_type' => $data['cat_filter_type'],
                'filter_type' => $data['filter_type'],
                'cat_filters' => $data['cat_filters'],
                'category' => $data['category'],
                'pr_filters' => $data['pr_filters'],
                'manufactorer' => $data['manufactorer'],
                'pages' => $data['pages'],
                'path' => $data['path'],
                'post' => $data['post'],
                'topPath' => 'top',
                'topName' => 'Лидеры продаж',
                'title' => 'Велокосмос - Лидеры продаж',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));


        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_test()
    {
        try {

            $this->view->generate('products/test.phtml', 'Template.phtml', array(

            ));


        } catch (Exception $e) {
            throw $e;
        }    }


    function action_manufacturer($id)
    {
        try {
            $this->model->db->connect();

            $id = explode('/', $id);

            $man_id = (int)$id[0];
            $data['manufactorer'] = $this->model->db->readWhere('manufactorer', 'id', $man_id);

            if ($data['manufactorer'] == null)
                throw new Exception404();

            $val = $this->model->db->getSettingsCount();
            $val = (int)$val['products_count'];

            $page = (int)$id[1];

            if ($page === 0 and $id[1] != '')
                throw new Exception404();

            if ($id[1] == null || $id[1] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[1]) - $val;
            }

            $data['man_category'] = $this->model->db->selectManCategorys($man_id);

            $data['category'] = $this->model->db->selectTable(array('category'));

            $count = $this->model->db->selectManProductsCount($man_id);
            $count = (int)$count['COUNT(*)'];

            $data['pages']['count'] = $count;
            $data['pages']['select'] = ($page == 0) ? 1 : $page;
            $data['pages']['size'] = $val;
            $data['pages']['page'] = "/products/manufacturer/$man_id/";

            $data['filter_type'] = $this->model->db->selectTable(array("filter_type"));

            $data['content'] = $this->model->db->selectManProducts($man_id, $with, $val);

            foreach ($data['content'] as $product) {
                $data['pr_filters'][$product['id']] = $this->model->productGetFilters($product);
            }

            $this->view->generate('products/man_list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'prices' => $data['price'],
                'cat_filter_type' => $data['cat_filter_type'],
                'filter_type' => $data['filter_type'],
                'cat_filters' => $data['cat_filters'],
                'category' => $data['category'],
                'man_category' => $data['man_category'],
                'pr_filters' => $data['pr_filters'],
                'manufactorer' => $data['manufactorer'],
                'pages' => $data['pages'],
                'path' => $data['path'],
                'post' => $data['post'],
                'topPath' => 'act',
                'topName' => 'Акции',
                'title' => 'Велокосмос - Аукционные товары',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));


        } catch (Exception $e) {
            throw $e;
        }
    }


}
