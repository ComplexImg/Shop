<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;

class Controller_admin extends \application\core\Controller
{

    function __construct()
    {
        $this->model = new \application\models\modelAdmin();
        $this->view = new \application\core\View();
    }


    function action_index()
    {
        $this->checkLogin();

        $this->view->generate('admin/main.phtml', 'admin/Template.phtml');
    }


    function action_editor($id)
    {
        try {
            $this->checkLogin();

            $id = explode('/', $id);

            if ($id[0] === null or $id[1] === null)
                throw new Exception404();

            $data = $this->model->editor($id);

            $this->view->generate('admin/editor.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'path' => $id[0],
                'header' => "Редактирование: {$id[0]}",
                'title' => 'Админпанель - редактирование'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_new($id)
    {
        try {
            $this->checkLogin();

            $id = explode('/', $id);
            if ($id[0] === null)
                throw new Exception404();

            $this->model->_new($id);

            $this->view->generate('admin/editor.phtml', 'admin/Template.phtml', array(
                'header' => "Добавление: {$id[0]}",
                'title' => 'Админпанель - добавление'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_list($id)
    {
        try {
            $this->checkLogin();

            $id = explode('/', $id);
            if ($id[0] == null)
                throw new Exception404();

            $data = $this->model->_list($id);

            $this->view->generate('admin/grid.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'list' => $id[0],
                'header' => "Списки: <a href='/admin/list/news'>Новости</a> <a href='/admin/list/articles'>Статьи</a> <a href='/admin/list/site_content'>Оформление страниц</a><br>Текущий: {$id[0]}",
                'title' => 'Админпанель - список'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_otz()
    {
        try {
            $this->checkLogin();

            $data = $this->model->_otz();

            $this->view->generate('admin/otz.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'title' => 'Админпанель - отзывы'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_orders()
    {
        try {
            $this->checkLogin();

            $data = $this->model->getOrders();

            $this->view->generate('admin/orders.phtml', 'admin/Template.phtml', array(
                'content' => $data['content']
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_order_info($id)
    {
        try {

            $this->checkLogin();

            $id = explode('/', $id);

            if ($id[0] == null)
                throw new Exception404();

            if ($_POST != null) {
                $this->model->updateOrder($id);
                header("Location: /admin/order_info/" . $id[0]);
            }


            $data = $this->model->getOrderInfo($id);

            $this->view->generate('admin/order_info.phtml', 'admin/Template.phtml', array(
                'content' => $data['content']
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_setting()
    {
        try {

            $this->checkLogin();

            if ($_POST != null) {
                $this->model->setting($_POST);
                header("Location: /admin/setting");
            }

            $this->model->db->connect();
            $data = $this->model->db->selectTable(array('setting'));
            $data = $data[0];

            $this->view->generate('admin/setting.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_exit()
    {
        try {
            session_name('loginData');

            session_start();

            $_SESSION['login'] = '';
            $_SESSION['pass'] = '';

            header("Location: /admin/login");

        } catch (Exception $e) {
            throw $e;
        }

    }

    function action_login()
    {
        try {

            $login = $_POST['login'];
            $pass = $_POST['pass'];

            if ($login != null and $pass != null) {
                $this->model->db->connect();

                $count = $this->model->db->getLogin();
                $count = $count['COUNT(*)'];

                if ($count < 3) {
                    $result = $this->model->login($_POST['login'], $_POST['pass']);

                    if ($result) {
                        session_name('loginData');
                        session_start();

                        $_SESSION['login'] = ($login & $pass);
                        $_SESSION['pass'] = ($login | $pass);
                        header("Location: /admin/");
                    } else {
                        $this->model->db->saveLogin(array($_POST['login'], $_POST['pass']));
                        header("Location: /admin/login");
                    }
                }
            }

            $this->view->generate('admin/login.phtml', 'errors/Template.phtml', array());
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_main_menu($id)
    {
        try {

            $this->checkLogin();

            $id = explode('/', $id);

            $page = urldecode($id[0]);

            $data = $this->model->mainMenu();

            $menu = $data['mainArray'];

            if ($_POST != null) {

                if ($_POST['hid'] == 'edit') {
                    $menu = $this->model->updateMenu($page, $_POST, $menu);

                    $this->model->db->updateMenu($menu);
                } elseif ($_POST['hid'] == 'add') {

                    $menu = $this->model->updateMenu2($page, $_POST, $menu);
                    $this->model->db->updateMenu($menu);
                }

                header("Location: /admin/main_menu/$id[0]");

            }

            $this->view->generate('admin/main_menu.phtml', 'admin/Template.phtml', array(
                'category' => $data['category'],
                'mainArray' => $data['mainArray'],
                'page' => $page

            ));
        } catch (Exception $e) {
            throw $e;
        }
    }

    function action_bad_login()
    {
        try {
            $this->model->db->connect();

            $data = $this->model->db->selectTable(array('login'));

            $this->view->generate('admin/bad_login.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    private function checkLogin()
    {
        try {
            session_name('loginData');
            session_start();

            $login = $_SESSION['login'];
            $pass = $_SESSION['pass'];

            if ($login != null and $pass != null) {

                $this->model->db->connect();

                $result = $this->model->db->login(md5($login), md5($pass));

                if (!$result)
                    throw new Exception404('BAD LOGIN');

            } else
                throw new Exception404('BAD LOGIN');


        } catch (Exception $e) {
            throw $e;
        }
    }


    ///////////////////////////


    function action_obl()
    {
        try {
            $this->checkLogin();

            $data = $this->model->obl($_POST);

            $this->view->generate('admin/obl.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_slider()
    {
        try {
            $this->checkLogin();

            $data = $this->model->slider($_POST);

            $this->view->generate('admin/slider.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_sam()
    {
        try {
            $this->checkLogin();

            $data = $this->model->sam($_POST);

            $this->view->generate('admin/sam.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_product_edit($id)
    {
        try {
            $this->checkLogin();

            $id = explode('/', $id);

            if ($id[0] == null)
                throw new Exception404();

            $data = $this->model->product_edit($id);

            $this->view->generate('admin/product_edit.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'filter_type' => $data['filter_type'],
                'cat_filter_type' => $data['cat_filter_type'],
                'manufactorer' => $data['manufactorer'],
                'product_img' => $data['product_img'],
                'category' => $data['category'],
                'oldFilters' => $data['oldFilters'],
                'header' => "<a href='/admin/product_list'>Список товаров</a> > Редактирование товара",
                'title' => 'Админпанель - редактирование товара'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_product_add($id)
    {
        try {
            $this->checkLogin();

            $data = $this->model->product_add($_POST);

            $this->view->generate('admin/product_edit.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'filter_type' => $data['filter_type'],
                'cat_filter_type' => $data['cat_filter_type'],
                'manufactorer' => $data['manufactorer'],
                'category' => $data['category'],
                'header' => "<a href='/admin/product_list'>Список товаров</a> > Новый товар",
                'title' => 'Админпанель - новый товар'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_product_list($id)
    {
        try {
            $this->checkLogin();

            $id = explode('/', $id);

            $data = $this->model->product_list($id);

            $this->view->generate('admin/product_list.phtml', 'admin/Template.phtml', array(
                'content' => $data['content'],
                'list' => $id[0],
                'header' => "Список товаров {$id[0]}",
                'title' => 'Админпанель - список товаров'
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_manufactorer()
    {
        try {
            $this->checkLogin();

            $data = $this->model->manufactorer($_POST);

            $this->view->generate('admin/manufactorer.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_cat_filter($id)
    {
        try {

            $this->checkLogin();

            $id = explode('/', $id);

            if ($id[0] === null)
                throw new \Exception404();

            $data = $this->model->cat_filter($_POST, $id[0]);

            $this->view->generate('admin/cat_filter.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_product_category()
    {
        try {
            $this->checkLogin();

            $data = $this->model->product_category($_POST);

            $this->view->generate('admin/product_category.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_filter_type()
    {
        try {
            $this->checkLogin();

            $data = $this->model->filter_type($_POST);

            $this->view->generate('admin/filter_type.phtml', 'admin/Template.phtml', array(
                'content' => $data
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }
}
