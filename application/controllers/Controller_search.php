<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;

class Controller_search extends \application\core\Controller
{

    function __construct()
    {
        $this->modelAjax = new \application\models\modelAjax();
        $this->model = new \application\models\modelAdmin();
        $this->view = new \application\core\View();
    }


    function action_index()
    {
        try {
            if ($_POST['value'] != null and $_POST['razd'] != null) {
                $data = $this->model->searchModel();

                switch ($_POST['razd']) {
                    case 'product':
                        $path = "/products/select";
                        break;
                    case 'news':
                        $path = "/list/select/news";
                        break;
                    case 'articles':
                        $path = "/list/select/articles";
                        break;
                    default :
                        throw new Exception('bad search! not correct filter');

                }
            }elseif($_POST['value'] != null and $_POST['razd'] == null){
                $_POST['razd'] = 'product';
                $data = $this->model->searchModel();
                $path = "/products/select";
            }


            $this->view->generate('search.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'title' => 'Велокосмос - Поиск',
                'path' => $path,
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }


}
