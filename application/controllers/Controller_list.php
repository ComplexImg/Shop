<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;
use \PDOException;

class Controller_list extends \application\core\Controller
{

    function __construct()
    {
        $this->modelAjax = new \application\models\modelAjax();
        $this->model = new \application\models\modelList();
        $this->view = new \application\core\View();
    }


    function action_index()
    {
        throw new Exception404;
    }


    function action_news($id)
    {
        try {
            $id = explode('/', $id);

            $this->model->db->connect();

            $val = $this->model->db->getSettingsCount();
            $val = (int)$val['news_count'];

            $count = $this->model->db->getListCount('news');
            $count = (int)$count['COUNT(*)'];

            $page = (int)$id[0];
            if ($page === 0 and $id[0] != '')
                throw new Exception404();

            if ($id[0] == null || $id[0] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[0]) - $val;
            }

            $data['content'] = $this->model->getPageList(array('news'), $with, $val);

            if ($data['content'] == null)
                throw new Exception404();

            $pages = array(
                'select' => ($page == 0) ? 1 : $page,
                'count' => $count,
                'size' => $val,
                'page' => '/list/news/'
            );


            $this->view->generate('list/list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'page' => 'Новости',
                'pages' => $pages,
                'title' => 'Велокосмос - Новости',
                'href' => 'news',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (Exception $e) {
            throw $e;
        }
    }


    function action_articles($id)
    {
        try {
            $id = explode('/', $id);

            $this->model->db->connect();

            $val = $this->model->db->getSettingsCount();
            $val = (int)$val['news_count'];

            $count = $this->model->db->getListCount('articles');
            $count = (int)$count['COUNT(*)'];

            $page = (int)$id[0];
            if ($page === 0 and $id[0] != '')
                throw new Exception404();

            if ($id[0] == null || $id[0] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[0]) - $val;
            }

            $data['content'] = $this->model->getPageList(array('articles'), $with, $val);

            if ($data['content'] == null)
                throw new Exception404();

            $pages = array(
                'select' => ($page == 0) ? 1 : $page,
                'count' => $count,
                'size' => $val,
                'page' => '/list/articles/'
            );


            $this->view->generate('list/list.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'page' => 'Статьи',
                'pages' => $pages,
                'title' => 'Велокосмос - Статьи',
                'href' => 'articles',
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function action_select($id)
    {
        try {
            $id = explode('/', $id);

            if ($id[0] == null || $id[1] == null)
                throw new Exception404();

            $pageName = ($id[0] === 'news') ? "Новости" : "Статьи";

            $this->model->db->connect();
            $data['content'] = $this->model->db->getSelected(array($id[0], (int)$id[1]));

            $this->view->generate('list/select.phtml', 'Template.phtml', array(
                'content' => $data['content'],
                'href' => $id[0],
                'page' => $pageName,
                'title' => 'Велокосмос - ' . $pageName . ' - ' . $data['content']['name'],
                'shop' => $this->modelAjax->getCartStr(),
                'manSlider' => $this->modelAjax->getManufactorerSlider()
            ));

        } catch (PDOException $e) {
            if ($e->getMessage() === 'no data')
                throw new Exception404();
            else
                throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
