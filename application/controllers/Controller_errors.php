<?php

namespace application\controllers;

class Controller_errors extends \application\core\Controller
{

	function action_404()
	{
		$this->view->generate('errors/404.phtml', 'errors/Template.phtml', array(
			'title' => 'Велокосмос - Ошибка 404',
		));
	}

	function action_500()
	{
		$this->view->generate('errors/500.phtml', 'errors/Template.phtml', array(
			'title' => 'Велокосмос - Ошибка на сервере',
		));
	}

	function action_not_work()
	{
		try {
			$this->view->generate('errors/not_work.phtml', 'errors/Template.phtml', array(
				'title' => 'Велокосмос - Сайт находится на тех обслуживании',
			));
		} catch (Exception $e) {
			throw $e;
		}
	}

}
