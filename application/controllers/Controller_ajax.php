<?php

namespace application\controllers;

use Exception;
use application\core\exceptions\Exception404;


class Controller_ajax extends \application\core\Controller{
    public $db = null;
    public $model = null;

    function __construct()
    {
        $this->model = new \application\models\modelAjax();
        $this->db = new \application\data\dbProducts();
    }


    function action_add_product()
    {
        try {
            $id = $_POST['id'];
            $count = $_POST['count'];
            $price = $_POST['price'];
            $article = $_POST['article'];

            if ($id != null and $count != null and $price != null) {

                session_start();
                $products = json_decode($_SESSION['products'], true);
                $productsArticles = json_decode($_SESSION['articles'], true);


                if ($count > 0) {
                    $productsArticles[$article] = array(
                        'id' => $id,
                        'price' => $price,
                        'count' => $count,
                    );
                    $products[$id] = array(
                        'article' => $article,
                        'price' => $price,
                        'count' => $count,
                    );
                } else {
                    unset($products[$id]);
                    unset($productsArticles[$article]);
                }

                $_SESSION['products'] = json_encode($products, true);
                $_SESSION['articles'] = json_encode($productsArticles, true);

                if (($_SESSION['products'] == null) or ($_SESSION['articles'] == null))
                    echo 'false';
                else
                    echo 'true';
            }


        } catch (Exception $e) {
            echo 'false';
        }
    }


    function action_get_product()
    {
        try {

            echo $this->model->getCartStr();

        } catch (Exception $e) {
            echo false;
        }
    }


    function action_check_order()
    {
        try {
            $data = $this->model->dataFormat->titleFormat($_POST);

            $str = $_POST['g-recaptcha-response'];

            $checkReCaprcha = $this->model->reCaptcha->check($str);

            if ($checkReCaprcha === true) {

                if ($this->model->dataFormat->checkCount($data)){

                    echo 'true';

                }else{
                    echo 'Ошибка при заполнении полей';
                }
            } else {
                echo 'Ошибка с капчей';
            }

        } catch (Exception $e) {
            echo false;
        }
    }


    function action_add_otz()
    {
        try {
            $str = $_POST['g-recaptcha-response'];

            $checkReCaprcha = $this->model->reCaptcha->check($str);

            if ($checkReCaprcha === true) {

                $data = $this->model->dataFormat->titleFormat($_POST);

                $count = (
                    strlen($data['name']) > 4 && strlen($data['name']) < 100 &&
                    strlen($data['bad']) > 10 && strlen($data['bad']) < 2000 &&
                    strlen($data['good']) > 10 && strlen($data['good']) < 2000 &&
                    strlen($data['text']) > 10 && strlen($data['text']) < 2000
                );

                if (!$count)
                    echo "bad count!";
                else {
                    $this->db = new \application\data\dbProducts();

                    $this->db->connect();
                    $this->db->addOtz($data);

                    echo "
                <div class='otz'>
                    <header>Ваш отзыв : {$data['name']}</header>
                    <article>
                        <div class='ratingValue'>
                            ";

                    for ($i = 1; $i <= 5; $i++) {
                        if ($i <= $data['reviewStars'])
                            echo "<img src='/resources/images/yes.png'>";
                        else
                            echo "<img src='/resources/images/no.png'>";
                    }
                    echo "
                        </div>
                        <div style='color: green'>
                            <b>Достоинства: </b> {$data['good']}
                        </div>

                        <div style='color: red;  padding: 10px 0;'>
                            <b>Недостатки: </b>{$data['bad']}
                        </div>
                         {$data['text']}
                    </article>
                    <footer>Только что</footer>
                </div>
                ";
                }

            } else {
                echo 'Произошла ошибка!';
            }

        } catch (Exception $e) {
            echo 'Произошла ошибка!';
            throw $e;
        }
    }


}