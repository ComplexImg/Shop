<?php

namespace application\data;

use PDOException;

class dbList extends dbClass
{

    public function getSettingsCount()
    {
        try {
            $stmt = $this->db->prepare("SELECT news_count FROM {$this->dbTables['setting']}");

            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getListCount($list)
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables[$list]}");

            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }



    public function getList(array $data, $with, $val)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables[$data[0]]} LIMIT $with, $val");
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getSelected(array $data)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables[$data[0]]} WHERE id = ?");
            $stmt->execute(array($data[1]));

            $result = $stmt->fetch();

            if ($result == null)
                throw new PDOException("no data");

            return $result;

        } catch (PDOException $e) {
            throw $e;
        }
    }






}