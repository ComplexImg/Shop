<?php

namespace application\data;

use \PDOException;
use \Exception;

class workWithListData
{

    public function getNewArray(array $valuesList, array $post)
    {
        try {

            $i = -1;
            $index = 0;
            $newArray = array();

            foreach ($post as $postValue) {

                if ($i === -1) {
                    $index = $postValue;
                    $i++;
                } elseif ($i === (count($valuesList) - 1)) {

                    $newArray[$index][$valuesList[$i]] = $postValue;
                    $i = -1;
                    foreach ($newArray[$index] as $val) {
                        if ($val === '') {
                            unset($newArray[$index]);
                            break;
                        }
                    }

                } else {
                    $newArray[$index][$valuesList[$i]] = $postValue;
                    $i++;
                }
            }

            return $newArray;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function writeNewArrayInDB($dbName, dbClass $db, array $oldData, array $newArray)
    {
        try {
            $db->connect();

            foreach ($oldData as $key => $man) {
                if (array_key_exists($man['id'], $newArray)) {
                    // update in db and delete from array
                    $db->_update($dbName, $man['id'], $newArray[$man['id']]);
                    unset($newArray[$man['id']]);
                } else {
                    // delete from db and array
                    $db->_delete($dbName, $man['id']);
                    unset($newArray[$man['id']]);
                }
            }

            //insert new
            foreach ($newArray as $man) {
                $db->_insert($dbName, $man);
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

}