<?php

namespace application\data;

use Exception;

class workWithFilters
{

    public function getSelectedCategodyFilters($category, array $post)
    {
        try {
            $resultArray = array();

            foreach ($post as $key => $value) {
                if (strpos($key, $category) === 0) {
                    $filterType = substr($key, strpos($key, "_") + 1, strrpos($key, "_") - 2);

                    $filterId = substr($key, strrpos($key, "_") + 1);

                    if (strpos($filterId, 'dop') !== false) {
                        if ($value !== '') {
                            $filterId = substr($filterId, 0, strpos($filterId, "-"));
                            $resultArray[$filterType][$filterId]['dop_value'] = $value;
                        }
                    } elseif ($value !== '') {
                        $resultArray[$filterType][$filterId]['value'] = $value;
                    }
                    //    echo "$filterType ::   $key <br>";
                }
            }

            return $resultArray;

        } catch (Exception $e) {
            throw $e;
        }
    }
}