<?php

namespace application\data;

use \PDO;
use \PDOException;

class dbClass
{


    protected $dbTables = array(
        "articles" => "articles",
        "product_img" => "product_img",
        "category" => "category",
        "cat_filter_type" => "cat_filter_type",
        "filter" => "filter",
        "filter_type" => "filter_type",
        "login" => "login",
        "manufactorer" => "manufactorer",
        "news" => "news",
        "obl_prices" => "obl_prices",
        "orders" => "orders",
        "otz" => "otz",
        "product" => "product",
        "samovivoz" => "samovivoz",
        "setting" => "setting",
        "site_content" => "site_content",
        "slider" => "slider",
    );


    protected $dbSetting = array(
        'username' => 'mysql',
        'password' => 'mysql',
        'db' => 'shop2', // shop2
        'host' => 'localhost',
        'driver_options' => array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        )
    );

    public $db = null;

    public final function connect()
    {
        try {
            $dsn = "mysql:host={$this->dbSetting['host']};dbname={$this->dbSetting['db']}";

            if ($this->db === null)
                $this->db = new PDO($dsn, $this->dbSetting['username'], $this->dbSetting['password'], $this->dbSetting['driver_options']);

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public final function read(array $id)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables[$id[0]]} WHERE id = ?");
            $result = $stmt->execute(array($id[1]));

            if (!$result)
                throw new PDOException('dbClass :: read');

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public final function readWhereWhatArray($table, $what ,array $where, array $value)
    {
        try {
            $strWhere = '';
            foreach ($where as $valWhere) {
                $strWhere .= " $valWhere = ? and";
            }

            $strWhere = substr($strWhere, 0, strlen($strWhere) - 3);


            $str = "SELECT $what FROM {$this->dbTables[$table]} WHERE $strWhere";


            $stmt = $this->db->prepare($str);
            $result = $stmt->execute($value);

            if (!$result)
                throw new PDOException('dbClass :: readWhereArray');

            return $stmt->fetchall();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public final function readWhere($table, $where, $value)
    {
        try {
            $str = "SELECT * FROM {$this->dbTables[$table]} WHERE $where = ?";
            $stmt = $this->db->prepare($str);
            $result = $stmt->execute(array($value));

            if (!$result)
                throw new PDOException('dbClass :: readWhere');

            return $stmt->fetchall();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function workSite()
    {
        try {
            $str = "SELECT work FROM {$this->dbTables['setting']}";
            $stmt = $this->db->prepare($str);
            $stmt->execute();

            $temp = $stmt->fetch();

            return (bool)$temp['work'];

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function selectTable(array $id)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables[$id[0]]}");
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectSiteContent(array $data)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables[$data[0]]} WHERE name = ?");
            $stmt->execute(array(
                $data[1]
            ));

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function _update($table, $id, array $data)
    {
        try {
            $str = "UPDATE {$this->dbTables[$table]} SET ";

            $count = count($data);
            $i = 0;
            foreach ($data as $key => $value) {
                if (++$i < $count) {
                    $str .= "$key = :$key, ";
                } else
                    $str .= "$key = :$key";
            }

            $str .= " WHERE id = $id";

            $stmt = $this->db->prepare($str);
            $result = $stmt->execute($data);

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function _delete($table, $id)
    {
        try {
            $str = "DELETE FROM {$this->dbTables[$table]} WHERE id = $id";

            $stmt = $this->db->prepare($str);
            $result = $stmt->execute();

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function _insert($table, array $data)
    {
        try {
            $str = "INSERT INTO {$this->dbTables[$table]} (";

            $count = count($data);
            $i = 0;
            $temp = '';
            foreach ($data as $key => $value) {
                if (++$i < $count) {
                    $str .= "$key, ";
                    $temp .= ":$key, ";
                } else {
                    $str .= "$key";
                    $temp .= ":$key";

                }
            }

            $str .= " ) VALUES (" . $temp . ")";

            $stmt = $this->db->prepare($str);
            $result = $stmt->execute($data);

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }
}