<?php

namespace application\data;

use PDOException;
use PDO;

class dbProducts extends dbClass
{

    public function getOrderId()
    {
        try {

            $stmt = $this->db->prepare("SELECT id FROM {$this->dbTables['orders']} ORDER BY date DESC");
            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    private function selectC_keys(array $data)
    {
        try {

            $stmt = $this->db->prepare("SELECT DISTINCT {$data['field']} FROM {$this->dbTables['product']} WHERE c_key = ?");
            $stmt->execute(array(
                $data['c_key']
            ));

            while ($row = $stmt->fetch())
                if ($row[$data['field']] != '')
                    $returnList[] = $row;

            return $returnList;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function selectCategoryValues(array $category)
    {
        try {
            $result = array();

            if ($category['radius'] == true) {
                $result['radius'] = $this->selectC_keys(array(
                    'field' => 'radius',
                    'c_key' => $category['c_key'],
                ));
            }
            if ($category['size'] == true) {
                $result['size'] = $this->selectC_keys(array(
                    'field' => 'size',
                    'c_key' => $category['c_key'],
                ));
            }
            if ($category['color'] == true) {
                $result['color'] = $this->selectC_keys(array(
                    'field' => 'color',
                    'c_key' => $category['c_key'],
                ));
            }
            if ($category['material'] == true) {
                $result['material'] = $this->selectC_keys(array(
                    'field' => 'material',
                    'c_key' => $category['c_key'],
                ));
            }
            $result['manufactorer'] = $this->selectC_keys(array(
                'field' => 'manufactorer',
                'c_key' => $category['c_key'],
            ));

            $stmt = $this->db->prepare("SELECT MIN(price) FROM {$this->dbTables['product']} WHERE c_key = ?");
            $stmt->execute(array(
                $category['c_key']
            ));
            $result['price']['min'] = $stmt->fetch();


            $stmt = $this->db->prepare("SELECT MAX(price) FROM {$this->dbTables['product']} WHERE c_key = ?");
            $stmt->execute(array(
                $category['c_key']
            ));
            $result['price']['max'] = $stmt->fetch();

            return $result;

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getPriceForCategory($cat_id)
    {
        try {
            $result = array();

            $stmt = $this->db->prepare("SELECT MIN(act), MIN(price),  MAX(price)  FROM {$this->dbTables['product']} WHERE cat_id = ?");
            $stmt->execute(array(
                $cat_id
            ));
            $values = $stmt->fetch();

            $result['min']['MIN(price)'] = (($values['MIN(act)'] < $values['MIN(price)'] and $values['MIN(act)'] != 0) ?  $values['MIN(act)'] : $values['MIN(price)']);
            $result['max']['MAX(price)'] = $values['MAX(price)'];

            return $result;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function selectAllProductsTable()
    {
        try {
            $stmt = $this->db->prepare("SELECT id, name, price FROM {$this->dbTables['product']}");
            $stmt->execute();

            return $stmt->fetchAll();;

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectFilters($id_filter_type)
    {
        try {
            $stmt = $this->db->prepare("SELECT DISTINCT value FROM {$this->dbTables['filter']} WHERE id_filter_type = ?");
            $stmt->execute(array($id_filter_type));

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getListCount($category)
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables['product']} WHERE cat_id = ?");

            $stmt->execute(array(
                $category
            ));

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectActProductsCount()
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables['product']} WHERE act > 0");

            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectManProductsCount($man_id)
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables['product']} WHERE man_id = ?");

            $stmt->execute(
                array($man_id)
            );

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectTopProductsCount()
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables['product']} WHERE act > 0");

            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectActProducts($with, $val)
    {
        try {
            $stmt = $this->db->prepare("SELECT id, name, price, act, img, man_id FROM {$this->dbTables['product']} WHERE act > 0 LIMIT $with, $val");

            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectManProducts($man_id, $with, $val)
    {
        try {
            $stmt = $this->db->prepare("SELECT id, name, price, act, img, man_id FROM {$this->dbTables['product']} WHERE man_id = ? LIMIT $with, $val");

            $stmt->execute(array($man_id));

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectManCategorys($man_id)
    {
        try {
            $stmt = $this->db->prepare("SELECT DISTINCT cat_id FROM {$this->dbTables['product']} WHERE man_id = ?");

            $stmt->execute(array($man_id));

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }



    public function selectTopProducts($with, $val)
    {
        try {
            $stmt = $this->db->prepare("SELECT id, name, price, act, img, man_id FROM {$this->dbTables['product']} WHERE top = 1 LIMIT $with, $val");

            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getCountForParams($cat_id, $params = null)
    {
        try {
            $str = "SELECT COUNT(*) FROM {$this->dbTables['product']} WHERE cat_id = ?" . $params['query'] . $params['products'];

            $stmt = $this->db->prepare($str);

            if ($params != null) {
                $params = array_merge(array($cat_id), $params['params']);

            } else {
                $params = array($cat_id);
            }
            $stmt->execute($params);

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function selectProductsTableByKey($cat_id, $params = null, array $pages)
    {
        try {
            $str = "SELECT id, name, price, act, img, man_id FROM {$this->dbTables['product']} WHERE cat_id = ?" . $params['query'] .$params['products']. "{$params['sort_query']} LIMIT {$pages['with']}, {$pages['size']}";

            $stmt = $this->db->prepare($str);

            if ($params != null)
                $params = array_merge(array($cat_id), $params['params']);
            else {
                $params = array($cat_id);
            }
            $stmt->execute($params);

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function getShoppingProduct($id)
    {
        try {
            $stmt = $this->db->prepare("SELECT id, name, price, img, act FROM {$this->dbTables['product']} WHERE id = ?");
            $stmt->execute(array($id));

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getSettingsCount()
    {
        try {
            $stmt = $this->db->prepare("SELECT products_count FROM {$this->dbTables['setting']}");

            $stmt->execute();

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function addOrder(array $data, array $post)
    {
        try {
            $place = ($post['type'] === 'Доставка на дом' ? $post['obl2'] : $post['obl3']);

            $stmt = $this->db->prepare("INSERT INTO {$this->dbTables['orders']} (type, fio, phone, comment, text, stat, address, place, email, ip) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array(
                $post['type'],
                $post['fio'],
                $post['phone'],
                $post['comments'],
                $data['text'],
                'Новый заказ',
                $data['address'],
                $place,
                $post['email'],
                $_SERVER['REMOTE_ADDR'],
            ));

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getOtz($product)
    {
        try {
            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables['otz']} WHERE product = ?");

            $stmt->execute(array(
                $product
            ));

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function addOtz(array $data)
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->dbTables['otz']} (product, name, good, bad, text, ball, ip) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $stmt->execute(array(
                (int)$data['product'],
                $data['name'],
                $data['good'],
                $data['bad'],
                $data['text'],
                (int)$data['reviewStars'],
                '127.0.0.1',
            ));

        } catch (PDOException $e) {
            throw $e;
        }
    }


}