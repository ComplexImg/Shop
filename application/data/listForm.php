<?php

namespace application\data;

class listForm extends formData
{

    public $filterProducts = array();

    public function getQueryString()
    {
        $result = array(
            'query' => '',
            'params' => array()
        );

        if (count($this->filterProducts) > 0) {

            $result['products'] = ' and id in (';
            foreach ($this->filterProducts as $product)
                $result['products'] .= "$product, ";

            $result['products'] = substr($result['products'], 0, strlen($result['products']) - 2) . ")";

        }


        if ($this->data['man_id'] != null) {
            $result['query'] .= " and man_id = ?";
            $result['params'][] = $this->data['man_id'];
        }

        if ($this->data['minPrice'] != null) {
            $result['query'] .= " and price >= ?";
            $result['params'][] = $this->data['minPrice'];
        }
        if ($this->data['maxPrice'] != null) {
            $result['query'] .= " and price <= ?";
            $result['params'][] = $this->data['maxPrice'];
        }

        switch ($this->data['sort']) {
            case 1 :
                $result['sort_query'] = '';
                break;
            case 2 :
                $result['sort_query'] = ' ORDER BY date DESC';
                break;
            case 3 :
                $result['sort_query'] = ' ORDER BY act DESC';
                break;
            case 4 :
                $result['sort_query'] = ' ORDER BY price';
                break;
            case 5 :
                $result['sort_query'] = ' ORDER BY price DESC';
                break;
            default:
                $result['sort_query'] = '';
        }


        return $result;
    }


    public function nullData()
    {
        foreach ($this->data as $value) {
            if ($value != null)
                return false;
        }
        return true;
    }


}
