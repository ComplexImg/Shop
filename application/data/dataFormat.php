<?php

namespace application\data;

class dataFormat
{

    public function checkCount(array $vars)
    {
        if ($vars['type'] === 'Доставка на дом'){

            return (
                strlen($vars['fio']) > 3 and strlen($vars['fio']) < 101 and
                strlen($vars['phone']) > 5 and strlen($vars['phone']) < 101 and
                strlen($vars['index']) > 4 and strlen($vars['index']) < 101 and
                strlen($vars['obl']) > 4 and strlen($vars['obl']) < 101 and
                strlen($vars['town']) > 4 and strlen($vars['town']) < 101 and
                strlen($vars['adress']) > 4 and strlen($vars['adress']) < 101 and
                strlen($vars['email']) > 5 and strlen($vars['email']) < 101
            );

        }elseif($vars['type'] === 'Самовывоз'){

            return (
                strlen($vars['fio']) > 3 and strlen($vars['fio']) < 101 and
                strlen($vars['phone']) > 5 and strlen($vars['phone']) < 101 and
                strlen($vars['email']) > 5 and strlen($vars['email']) < 101
            );

        }else
            return false;
    }

    public function titleFormat(array $vars)
    {
        $result = array();

        foreach ($vars as $key => $value) {
            if($key != 'editor' and $key != 'har'){
                $value = trim($value);
                $value = strip_tags($value);
            }
            $result[$key] = $value;
        }

        return $result;
    }

    public function notNull(array $vars)
    {
        foreach ($vars as $value) {
            if ($value === ''){
                return false;
            }
        }
        return true;
    }

    public function deleteSl(array $array)
    {
        $newArray = array();

        foreach ($array as $key => $value){
            $value = trim($value);
            $value = strip_tags($value);
            $newArray[$key] = str_replace('/', "", $value);
            $newArray[$key] = str_replace('\\', "", $newArray[$key]);
        }
        return $newArray;
    }

}