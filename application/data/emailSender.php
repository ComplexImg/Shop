<?php

namespace application\data;

class emailSender
{

    private $adminEmail = "shop@velokosmos.ru";
    private $number;
    private $data;
    private $table;

    public function __construct($number, array $data, array $table)
    {
        $this->number = $number;
        $this->data = $data;
        $this->table = $table;
    }


    public function sendEmailForAdmin()
    {
        $to = $this->adminEmail;
        $subject = "В магазине «Велокосмос» был оформлен заказ";
        $message = $this->getEmailForAdministration();

        return $this->send($to, $subject, $message);
    }


    public function sendEmailForUser()
    {
        $to = trim($this->data['email']);
        $subject = "В магазине «Велокосмос» был оформлен заказ";
        $message = $this->getEmailForUser();

        return $this->send($to, $subject, $message);
    }


    private function send($to, $subject, $message)
    {
        $message = wordwrap($message, 70, "\r\n");

        $headers = "Content-type: text/html; charset=UTF-8 \r\n";
        $headers .= "From: Магазин «Велокосмос» <info@velokosmos.ru>\r\n";

        return mail($to, $subject, $message, $headers);
    }


    private function getMailBody()
    {
        $data = $this->data;
        $table = $this->table;
        $mail = '';

        $mail .= $this->getInfo();
        $mail .= "<br><hr><br>";
        $mail .= $this->getTable();
        $mail .= "<br><hr>";

        if ($data['type'] === 'Самовывоз') {
            $price = (int)$table['content']['total'];
        } else {
            $priceDost = $data['obl2'];
            $priceDost = substr($priceDost, strrpos($priceDost, "- ") + 2);
            $priceDost = substr($priceDost, 0, strrpos($priceDost, "руб"));
            $price = (int)$table['content']['total'] + (int)$priceDost;
        }

        $mail .= "<h3>Итоговая стоимость: {$price},00 руб</h3>";

        return $mail;
    }


    private function getTable()
    {
        $str = ' 
    <table style="width: 100%">
        <tr>
            <td><b>№</b></td>
            <td><b>Наименование</b></td>
            <td><b>Артикул</b></td>
            <td><b>Кол-во</b></td>
            <td><b>Цена</b></td>
            <td><b>Сумма</b></td>
        </tr>';

        $i = 1;

        foreach ($this->table['content'] as $value) {
            if (is_array($value)) {
                $str .= " 
        <tr>
            <td>{$i}</td>
            <td>{$value['name']}</td>
            <td>{$value['article']}</td>
            <td>{$value['count']}</td>
            <td>". (($value['act'] !== '0') ? $value['act'] : $value['price']) .",00 руб.</td>
            <td>{$value['totalprice']},00 руб.</td>
        </tr>";
                $i++;
            }
        }

        $data = $this->data;

        if ($data['type'] === 'Самовывоз') {
            $str .= " 
        <tr>
            <td>{$i}</td>
            <td>{$data['type']}</td>
            <td>-</td>
            <td>1</td>
            <td>0,00 руб</td>
            <td>0,00 руб</td>
        </tr>";

        } else {
            $price = $data['obl2'];
            $price = substr($price, strrpos($price, "- ") + 2);
            $price = substr($price, 0, strrpos($price, "руб"));

            $str .= " 
        <tr>
            <td>{$i}</td>
            <td>Доставка Курьером</td>
            <td>-</td>
            <td>1</td>
            <td>$price,00 руб</td>
            <td>$price,00 руб</td>
        </tr>";
        }

        $str .= '</table>';
        return $str;
    }


    private function getInfo()
    {
        $data = $this->data;

        $str = "
    <b>ФИО:</b> {$data['fio']}<br>
    <b>E-mail:</b> {$data['email']}<br>
    <b>Телефон:</b> {$data['phone']}<br>
    <b>Тип доставки:</b> {$data['type']}<br>
    <b>Место:</b> " . (($data['type'] === 'Самовывоз') ? $data['obl2'] : $data['obl3']) . "<br>
    <b>Комментарий к заказу:</b> {$data['comments']}<br>
    ";

        if ($data['index'] !== '')
            $str .= "<b>Индекс: </b> {$data['index']}<br>";

        if ($data['obl'] !== '')
            $str .= "<b>Край \ область: </b> {$data['obl']}<br>";

        if ($data['town'] !== '')
            $str .= "<b>Город \ населенный пункт: </b> {$data['town']}<br>";

        if ($data['adress'] !== '')
            $str .= "<b>Полный адрес: </b> {$data['adress']}<br>";

        return $str;
    }


    private function getEmailForAdministration()
    {

        $mail = "<h3>В магазине «Велокосмос» был оформлен заказ №{$this->number}</h3><hr><br>";

        $mail .= $this->getMailBody();

        return $mail;

    }


    private function getEmailForUser()
    {

        $mail = "<h3>В магазине «Велокосмос» был оформлен заказ №{$this->number}</h3><hr><br>
В ближайшее время с Вами свяжется наш специалист.<br>
    Пожалуйста, проверьте правильность заполнения Вашей контактной информации:
    <br>
    <br>";

        $mail .= $this->getMailBody();

        $mail .= "<br>
    По всем вопросам Вы можете связаться с нами по телефону: +7(921)551-56-85
    <br><br>
    Благодарим за выбор нашего магазина!
    <br><br>
    Команда Велокосмос";

        return $mail;

    }

}