<?php

namespace application\data;

use Exception;

class reCaptcha{

    private $secret = '6LfUFioUAAAAAA9FynlNjMAei03Crvbyceve1yoz';

    public function check($str)
    {
        try{
            $postdata = http_build_query(
                array(
                    'secret' => $this->secret,
                    'response' => $str
                //    'response' => $post['g-recaptcha-response']
                )
            );

            $opts = array('http' =>
                array(
                    'method' => 'POST',
                    'content' => $postdata,
                    'ignore_errors' => '1'
                )
            );

            $context = stream_context_create($opts);

            $result = file_get_contents("https://www.google.com/recaptcha/api/siteverify", false, $context);

            $result = json_decode($result, true);

            return $result['success'];

        }catch (Exception $e){
            throw $e;
        }
    }

}