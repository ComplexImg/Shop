<?php

namespace application\data;

use PDOException;

class dbAdmin extends dbClass
{


    public function selectRandWhereLimit($table, $where, $limit)
    {
        try {
            $str = "SELECT id, img, name, price, act FROM {$this->dbTables[$table]} WHERE $where  ORDER BY RAND() LIMIT $limit";

            $stmt = $this->db->prepare($str);
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (Exception404 $e) {
            throw $e;
        }
    }


    public function selectFieldsForMain($table, $count)
    {
        try {

            $stmt = $this->db->prepare("SELECT id, img, name FROM {$this->dbTables[$table]} ORDER BY id DESC LIMIT $count");
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (Exception404 $e) {
            throw $e;
        }
    }


    public function updateMenu($json)
    {
        try {
            $str = "UPDATE {$this->dbTables['setting']} SET `main_menu`= ?";
            $stmt = $this->db->prepare($str);
            $result = $stmt->execute(array($json));

            if (!$result)
                throw new PDOException('dbAdmin :: updateMenu');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function updatePageables($pr, $nw, $work, $secret)
    {
        try {
            $str = "UPDATE {$this->dbTables['setting']} SET products_count= ?, news_count= ?, work = ? WHERE secret = ?";
            $stmt = $this->db->prepare($str);
            $result = $stmt->execute(array(
                $pr,
                $nw,
                $work,
                $secret
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: updatePageables');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function save(array $id, array $data)
    {
        try {
            $stmt = $this->db->prepare("UPDATE {$this->dbTables[$id[0]]} SET `name`= ?,`text`= ?,`img`= ? WHERE id = ?");
            $result = $stmt->execute(array(
                $data['header'],
                $data['editor'],
                $data['img'],
                $id[1]));

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function selectProductsTable(array $id)
    {
        try {
            $str = "SELECT * FROM {$this->dbTables['product']} WHERE c_key = ?";
            $stmt = $this->db->prepare($str);
            $stmt->execute(array(
                $id[0]
            ));

            while ($row = $stmt->fetch())
                $returnList[] = $row;

            return $returnList;

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function add(array $id, array $data)
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->dbTables[$id[0]]} (name, text) VALUES (?, ?)");
            $result = $stmt->execute(array(
                $data['header'],
                $data['editor']
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function delete(array $id, array $data)
    {
        try {
            $stmt = $this->db->prepare("DELETE FROM {$this->dbTables[$id[0]]} WHERE id = ?");
            $result = $stmt->execute(array($data['deleted']));

            if (!$result)
                throw new PDOException('dbAdmin :: deleted');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function saveProduct(array $id, array $data)
    {
        try {
            $stmt = $this->db->prepare("UPDATE {$this->dbTables['product']} SET `top`= ?, `name`= ?, `cat_id`= ?, `price`= ?, `man_id`= ?, `text`= ?, `act`= ?, `tags`= ?, `img`= ?, `har`= ? WHERE id = ?");
            $result = $stmt->execute(array(
                $data['top'],
                $data['name'],
                $data['category'],
                (int)$data['price'],
                $data['manufactorer'],
                $data['editor'],
                (int)$data['act'],
                $data['tags'],
                $data['img'],
                $data['har'],
                $id[0],
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: save');


        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function addProduct(array $data)
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->dbTables['product']} (top, name, cat_id, price, man_id, text, act, tags, img, har) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $result = $stmt->execute(array(
                $data['top'],
                $data['name'],
                $data['category'],
                (int)$data['price'],
                $data['manufactorer'],
                $data['editor'],
                (int)$data['act'],
                $data['tags'],
                $data['img'],
                $data['har'],
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: addProduct');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function search(array $data)
    {
        try {
            if ($data[0] === 'product')
                $str = "SELECT id, name, date, img FROM {$this->dbTables[$data[0]]} WHERE text LIKE '%{$data[1]}%' or tags LIKE '%{$data[1]}%' or name LIKE '%{$data[1]}%';";
            else
                $str = "SELECT id, name, date, img FROM {$this->dbTables[$data[0]]} WHERE text LIKE '%{$data[1]}%' or name LIKE '%{$data[1]}%';";

            $stmt = $this->db->prepare($str);
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function updateOrder(array $data, array $id)
    {
        try {
            $stmt = $this->db->prepare("UPDATE {$this->dbTables['orders']} SET `stat`= ?,`comment`= ? WHERE id = ?");
            $result = $stmt->execute(array(
                $data['stat'],
                $data['comments'],
                $id[0]
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: save');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function allOrders()
    {
        try {

            $stmt = $this->db->prepare("SELECT id, type, stat, date, ip FROM {$this->dbTables['orders']}");
            $stmt->execute();

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function login($login, $pass)
    {
        try {

            $stmt = $this->db->prepare("SELECT * FROM {$this->dbTables['setting']} WHERE login = ? and pass = ?");
            $stmt->execute(array(
                $login,
                $pass
            ));

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function saveLogin(array $data)
    {
        try {
            $stmt = $this->db->prepare("INSERT INTO {$this->dbTables['login']} (login, pass, ip) VALUES(?, ?, ?)");
            $result = $stmt->execute(array(
                $data[0],
                $data[1],
                $_SERVER['REMOTE_ADDR']
            ));

            if (!$result)
                throw new PDOException('dbAdmin :: saveLogin');

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function getLogin()
    {
        try {
            $stmt = $this->db->prepare("SELECT COUNT(*) FROM {$this->dbTables['login']} WHERE ip = ? and date = CURDATE()");
            $stmt->execute(array(
                $_SERVER['REMOTE_ADDR']
            ));

            return $stmt->fetch();

        } catch (PDOException $e) {
            throw $e;
        }
    }

}