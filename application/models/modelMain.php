<?php

namespace application\models;


use Exception;
use application\core\exceptions\Exception404;

class modelMain extends \application\core\Model
{

    public $db = null;
    public $dataFormat;

    public function __construct()
    {
        $this->db = new \application\data\dbAdmin();
        $this->dataFormat = new \application\data\dataFormat();
    }

    public function getSlider()
    {
        try {
            $this->db->connect();

            $data = $this->db->selectTable(array('slider'));

            $answer = '';

            $i = 1;
            $count = count($data);
            foreach ($data as $key => $value) {
                $ch = ($i === 1) ? "checked='checked'" : "";
                $answer .= "<input type='radio' name='slider2' id='slider2_{$i}' $ch>
                <label for='slider2_{$i}'></label>
                <div>
                    <p>{$value['title']}</p>
                    <img src='/resources/imagebrowser/slider/{$value['img']}' alt=''>
                </div>
                ";
                if ($i == $count) {
                    $i = 1;
                } else {
                    $i++;
                }

                $answer .= "<label for='slider2_{$i}'></label>";
            }

            return $answer;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getRandActProducts($count)
    {
        try {

            $this->db->connect();

            $data = $this->db->selectRandWhereLimit('product', 'act <> 0', $count);

            $answer = '';
            $i = 0;

            if ($data != null) {
                foreach ($data as $pData) {
                    $answer .= "<a href='/products/select/{$pData['id']}' class='{$i}'>
                                    <div class='ablock'>
                                        <img src='/resources/imagebrowser/products/mini/{$pData['img']}'>
                                        <br>
                                        {$this->nameLength($pData['name'])}
                                    </div>
                                </a>";
                    $i++;
                }
            }
            return $answer;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getTopProducts($count)
    {
        try {

            $this->db->connect();

            $data = $this->db->selectRandWhereLimit('product', 'top = 1', $count);

            $answer = '';
            $i = 0;


            if ($data != null) {
                foreach ($data as $pData) {
                    $answer .= "<a href='/products/select/{$pData['id']}' class='{$i}'>
                                    <div class='ablock'>
                                        <img src='/resources/imagebrowser/products/mini/{$pData['img']}'>
                                        <br>
                                        {$this->nameLength($pData['name'])}
                                    </div>
                                </a>";
                    if ($i === 3)
                        break;
                    $i++;
                }
            }
            return $answer;

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getMainData($table, $count)
    {
        try {

            $this->db->connect();

            $data = $this->db->selectFieldsForMain($table, $count);

            $answer = '';
            $i = 0;

            if ($data != null) {
                foreach ($data as $pData) {
                    $answer .= "<a href='/list/select/{$table}/{$pData['id']}' class='{$i}'>
                                    <div class='ablock'>
                                        <img src='/resources/imagebrowser/{$table}/mini/{$pData['img']}'>
                                        <br>
                                        {$this->nameLength($pData['name'])}
                                    </div>
                                </a>";
                    $i++;
                }
            }

            return $answer;

        } catch (Exception $e) {
            throw $e;
        }
    }

    private function nameLength($name)
    {
        if (strlen($name) > 40) {
            for ($i = strlen($name) - 1; $i > 1; $i--){
                if ($name[$i] === ' ' and $i < 40)
                    return substr($name, 0, $i) . '...';
            }
            return $name;
        } else
            return $name;
    }

    public function getMainArray()
    {
        try {
            $this->db->connect();

            $result = $this->db->selectTable(array('setting'));

            $json = $result[0]['main_menu'];

            if ($json == null)
                throw new Exception('NO DATA');

            return json_decode($json, true);

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getMainMenu(array $menuArray)
    {
        require_once "{$GLOBALS['path']}/kendo/lib/Kendo/Autoload.php";

        $menu = new \Kendo\UI\Menu('verticalMenu');
        $menu->orientation('vertical');
        $menu->attr('class', 'vertMenu');

        foreach ($menuArray as $name => $array) {
            $item = new \Kendo\UI\MenuItem($name);
            $item->url("/main/menu/" . $name);
            foreach ($array as $key => $path) {
                $it1 = new \Kendo\UI\MenuItem($key);
                $it1->url("/products/list/" . $path);
                $item->addItem($it1);
            }
            $menu->addItem($item);
        }
        return $menu->render();
    }

}
