<?php

namespace application\models;

use Exception;
use application\core\exceptions\Exception404;

class modelList extends \application\core\Model
{

    public $db = null;

    public function __construct()
    {
        $this->db = new \application\data\dbList();

    }


    public function getPageList(array $data, $with, $val)
    {
        try{
            $this->db->connect();

            $data = $this->db->getList(array($data[0]), $with, $val);

            return $data;

        }catch (Exception $e){
            throw $e;
        }
    }


}
