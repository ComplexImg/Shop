<?php

namespace application\models;

use Exception;
use application\core\exceptions\Exception404;

class modelAdmin extends \application\core\Model
{

    public $db = null;
    public $dataFormat = null;
    public $filterWorker = null;
    public $listDataWorker = null;

    public function __construct()
    {
        $this->db = new \application\data\dbAdmin();
        $this->dataFormat = new \application\data\dataFormat();
        $this->filterWorker = new \application\data\workWithFilters();
        $this->listDataWorker = new \application\data\workWithListData();
    }


    public function setting($data)
    {
        try {

            $this->db->connect();

            $pr = (int)$data['products_count'];
            $nw = (int)$data['news_count'];

            $this->db->updatePageables(
                ($pr != 0) ? $pr : 10,
                ($nw != 0) ? $nw : 4,
                (bool)$_POST['work'],
                md5($_POST['secret'])
            );

        } catch (Exception404 $e) {
            throw $e;
        }
    }


    public function _list($id)
    {
        try {
            $data = array();
            $this->db->connect();

            if ($_POST['deleted'] != null) {
                $this->db->delete($id, $_POST);
                header("Location: /admin/list/{$id[0]}");
            }

            $data['content'] = $this->db->selectTable($id);

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function _otz()
    {
        try {
            $data = array();
            $this->db->connect();

            if ($_POST['deleted'] != null) {
                $this->db->delete(array('otz'), $_POST);
                header("Location: /admin/otz");
            }

            $data['content'] = $this->db->selectTable(array('otz'));

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function _new($id)
    {
        try {

            if ($_POST['editor'] != null and $_POST['header'] != null) {
                $this->db->connect();

                $values = $this->dataFormat->titleFormat($_POST);

                $this->db->add($id, $values);

                header("Location: /admin/list/{$id[0]}");
            }

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function editor($id)
    {
        try {
            $data = array();
            $this->db->connect();

            if ($_POST['editor'] != null and $_POST['header'] != null) {

                $values = $this->dataFormat->titleFormat($_POST);

                $this->db->save($id, $values);
                header("Location: /admin/list/{$id[0]}");
            }

            $data['content'] = $this->db->read($id);
            if (!$data['content'])
                throw new Exception404();

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function searchModel()
    {
        try {
            $data = array();
            $this->db->connect();

            $value = $_POST['value'];
            $razd = $_POST['razd'];

            $data['content'] = $this->db->search(array($razd, $value));

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getOrders()
    {
        try {
            $data = array();
            $this->db->connect();

            if ($_POST['deleted'] != null) {
                $this->db->delete(array('orders'), $_POST);
                header("Location: /admin/orders");
            }

            $data['content'] = $this->db->allOrders();

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function updateOrder($id)
    {
        try {
            $data = array();
            $this->db->connect();

            $values = $this->dataFormat->titleFormat($_POST);
            $this->db->updateOrder($values, $id);

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getOrderInfo($id)
    {
        try {
            $data = array();
            $this->db->connect();

            $data['content'] = $this->db->read(array('orders', $id[0]));

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function login($login, $pass)
    {
        try {
            $newPass = ($login | $pass);
            $newPass = md5($newPass);

            $newLogin = ($login & $pass);
            $newLogin = md5($newLogin);


            return $this->db->login($newLogin, $newPass);

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function mainMenu()
    {
        try {
            $this->db->connect();

            $data['category'] = $this->db->selectTable(array('category'));


            $result = $this->db->selectTable(array('setting'));

            $json = $result[0]['main_menu'];

            if ($json == null)
                throw new Exception('NO DATA');

            $data['mainArray'] = json_decode($json, true);

            return $data;

        } catch (Exception $e) {
            throw $e;
        }

    }

    public function updateMenu($name, $post, $old)
    {
        try {
            $newArray = array();
            $i = 0;
            $temp = '';
            foreach ($post as $key => $value) {
                if ($key != 'hid') {
                    if ($i == 0) {
                        $temp = $value;
                        $i = 1;
                    } else {
                        if ($temp != null and $value != null)
                            $newArray[$temp] = $value;
                        $i = 0;
                    }
                }
            }
            $old[$name] = $newArray;
            return json_encode($old, true);


        } catch (Exception $e) {
            throw $e;
        }
    }


    public function updateMenu2($name, $post, $old)
    {
        try {
            $newArray = array();
            $temp = '';

            $i = 1;
            foreach ($old as $key => $value) {

                $j = 0;
                foreach ($post as $pKey => $pValue) {
                    if ($pKey != 'hid' and $i === $j and $pValue != '') {
                        $newArray[$pValue] = $value;
                        $temp = $pValue;
                    }
                    $j++;
                }
                $i++;
            }

            $nam = "main_" . (count($post) - 2);

            if ($post[$nam] != '')
                $newArray[$post[$nam]] = array();

            return json_encode($newArray, true);


        } catch (Exception $e) {
            throw $e;
        }
    }


    ///////////////////////////////////////////

    public function product_edit($id)
    {
        try {
            $data = array();
            $this->db->connect();

            $data['category'] = $this->db->selectTable(array('category'));
            $data['filter_type'] = $this->db->selectTable(array("filter_type"));
            $data['cat_filter_type'] = $this->db->selectTable(array("cat_filter_type"));
            $data['manufactorer'] = $this->db->selectTable(array("manufactorer"));
            $data['oldFilters'] = $this->db->readWhere('filter', 'id_product', $id[0]);
            $data['product_img'] = $this->db->readWhere("product_img", 'id_product', $id[0]);

            $oldFilters = array();

            $newOldFilters = array();
            foreach ($data['oldFilters'] as $filter) {
                $newOldFilters[$filter['id_filter_type']][$filter['id']] = $filter;
            }


            foreach ($data['oldFilters'] as $value) {
                $oldFilters[$value['id']] = $value['value'];
            }

            if ($_POST != null) {
                $filters = $this->filterWorker->getSelectedCategodyFilters($_POST['category'], $_POST);


                foreach ($newOldFilters as $oldFiltId => $oldFiltVal) {
                    foreach ($filters[$oldFiltId] as $filId => $arr) {
                        if (array_key_exists($filId, $oldFiltVal)) {

                            $newData = array(
                                'value' => $arr['value'],
                                'dop_value' => $arr['dop_value'],
                                'id_category' => $_POST['category']
                            );

                            $this->db->_update('filter', $filId, $newData);

                            unset($newOldFilters[$oldFiltId][$filId]);
                            unset($filters[$oldFiltId][$filId]);

                        }
                    }
                }

                foreach ($newOldFilters as $oldFiltId => $oldFiltVal) {
                    foreach ($oldFiltVal as $key => $val) {

                        $this->db->_delete("filter", $key);
                    }
                }


                foreach ($filters as $filterId => $filterValues) {
                    foreach ($filterValues as $value) {
                        $fil = array(
                            'value' => $value['value'],
                            'dop_value' => $value['dop_value'],
                            'id_filter_type' => $filterId,
                            'id_product' => $id[0],
                            'id_category' => $_POST['category']
                        );

                        $this->db->_insert('filter', $fil);
                    }
                }


// img -----------------------------
                $postImgArray = array();
                foreach ($_POST as $key => $value) {
                    if (strpos($key, "id_") === 0) {
                        $postImgArray[$key] = $value;
                    } elseif (strpos($key, "img_") === 0) {
                        $postImgArray[$key] = $value;
                    } elseif (strpos($key, "other_") === 0) {
                        $postImgArray[$key] = $value;
                    } elseif ((strpos($key, "stat_") === 0) and strpos($key, "input") === false)
                        $postImgArray[$key] = $value;
                }

                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'img',
                        'article',
                        'stat',
                    ),
                    $postImgArray);


                foreach ($data['product_img'] as $man) {
                    if (array_key_exists($man['id'], $newArray)) {
                        // update in db and delete from array

                        $this->db->_update('product_img', $man['id'], $newArray[$man['id']]);
                        unset($newArray[$man['id']]);
                    } else {
                        // delete from db and array
                        $this->db->_delete('product_img', $man['id']);
                        unset($newArray[$man['id']]);
                    }
                }

                //insert new
                foreach ($newArray as $man) {
                    $man['id_product'] = $id[0];
                    $this->db->_insert('product_img', $man);
                }


//----------------------------img

                $values = $this->dataFormat->titleFormat($_POST);
                $this->db->saveProduct($id, $values);

                header("Location: /admin/product_list");
            }

            $data['content'] = $this->db->read(array('product', $id[0]));
            if (!$data['content'])
                throw new Exception404();

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function product_add($post)
    {
        try {
            $this->db->connect();

            $data['category'] = $this->db->selectTable(array('category'));
            $data['filter_type'] = $this->db->selectTable(array("filter_type"));
            $data['cat_filter_type'] = $this->db->selectTable(array("cat_filter_type"));
            $data['manufactorer'] = $this->db->selectTable(array("manufactorer"));

            if ($post != null) {
                $values = $this->dataFormat->titleFormat($post);
                $this->db->addProduct($values);
                header("Location: /admin/product_list");
            }
            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function product_list()
    {
        try {
            $data = array();
            $this->db->connect();

            if ($_POST['deleted'] != null) {
                $this->db->delete(array('product'), $_POST);
                header("Location: /admin/product_list");
            }
            $data['category'] = $this->db->selectTable(array('category'));
            $data['content'] = $this->db->selectTable(array('product'));

            foreach ($data['content'] as $key => $value) {
                $cat_id = $data['content'][$key]['cat_id'];
                $act = $data['content'][$key]['act'];
                if ($act != '0') {
                    $data['content'][$key]['price'] = $act . ' (акция)';
                }

                $data['content'][$key]['c_key'] = 'Ошибка!';
                foreach ($data['category'] as $cat) {
                    if ($cat['id'] === $cat_id) {
                        $data['content'][$key]['c_key'] = $cat['name'];
                        break;
                    }
                }
            }

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function slider(array $post)
    {
        try {
            $this->db->connect();
            $date = $this->db->selectTable(array("slider"));

            if ($_POST != null) {

                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'title',
                        'path',
                        'text',
                        'img'
                    ),
                    $post);

                $this->listDataWorker->writeNewArrayInDB('slider', $this->db, $date, $newArray);
                header("Location: /admin/slider");

            }

            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function sam(array $post)
    {
        try {
            $this->db->connect();
            $date = $this->db->selectTable(array("samovivoz"));

            if ($_POST != null) {

                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'place',
                        'time',
                        'other'
                    ),
                    $post);

                $this->listDataWorker->writeNewArrayInDB('samovivoz', $this->db, $date, $newArray);
                header("Location: /admin/sam");

            }

            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function obl(array $post)
    {
        try {
            $this->db->connect();
            $date = $this->db->selectTable(array("obl_prices"));

            if ($_POST != null) {

                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'name',
                        'price',
                        'text'
                    ),
                    $post);

                $this->listDataWorker->writeNewArrayInDB('obl_prices', $this->db, $date, $newArray);
                header("Location: /admin/obl");
            }
            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function manufactorer(array $post)
    {
        try {
            $this->db->connect();

            $date = $this->db->selectTable(array("manufactorer"));

            $postNew = array();
            foreach ($post as $key => $value) {
                if (strpos($key,"_input") === false)
                    $postNew[$key] = $value;
            }

            if ($_POST != null) {
                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'name',
                        'text',
                        'img',
                        'view'
                    ),
                    $postNew);

                 $this->listDataWorker->writeNewArrayInDB('manufactorer', $this->db, $date, $newArray);
                  header("Location: /admin/manufactorer");
            }
            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function product_category(array $post)
    {
        try {

            $this->db->connect();
            $date = $this->db->selectTable(array("category"));

            if ($_POST != null) {
                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'name',
                        'c_key',
                        'img'
                    ),
                    $post);

                $this->listDataWorker->writeNewArrayInDB('category', $this->db, $date, $newArray);
                header("Location: /admin/product_category");
            }
            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function filter_type(array $post)
    {
        try {
            $this->db->connect();

            $newPost = array();
            foreach ($_POST as $key => $value) {
                if (strpos($key, "input") === false)
                    $newPost[$key] = $value;
            }


            $date = $this->db->selectTable(array("filter_type"));

            if ($post != null) {
                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'name',
                        'other',
                        'view'
                    ),
                    $newPost);

                $this->listDataWorker->writeNewArrayInDB('filter_type', $this->db, $date, $newArray);
                header("Location: /admin/filter_type");

            }
            return $date;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function cat_filter(array $post, $id)
    {
        try {

            $this->db->connect();

            $data['cat_info'] = $this->db->read(array('category', $id));

            if ($data['cat_info'] == null)
                throw new Exception404();

            $data['cat_filter'] = $this->db->readWhere('cat_filter_type', 'id_category', $id);

            $data['all_filter'] = $this->db->selectTable(array("filter_type"));
            $data['all_filter'][] = array(
                'id' => "",
                'name' => ""
            );


            if ($post != null) {
                $newArray = $this->listDataWorker->getNewArray(
                    array(
                        'id_filter_type'
                    ),
                    $post);


                foreach ($data['cat_filter'] as $key => $man) {
                    if (array_key_exists($man['id'], $newArray)) {
                        // update this filter_type ind db and delete from array
                        $this->db->_update('cat_filter_type', $man['id'], $newArray[$man['id']]);
                        unset($newArray[$man['id']]);
                    } else {
                        // delete this filter_type from db and array
                        $this->db->_delete('cat_filter_type', $man['id']);
                        unset($newArray[$man['id']]);
                    }
                }

                //insert new filter_type
                foreach ($newArray as $man) {
                    $man['id_category'] = $id;
                    $this->db->_insert('cat_filter_type', $man);
                }

                header("Location: /admin/cat_filter/$id");
            }

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }
}
