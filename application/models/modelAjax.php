<?php

namespace application\models;

use Exception;
use application\core\exceptions\Exception404;

class modelAjax extends \application\core\Model
{
    public $dataFormat = null;
    public $reCaptcha = null;
    public $db = null;

    public function __construct()
    {
        $this->reCaptcha = new \application\data\reCaptcha();
        $this->dataFormat = new \application\data\dataFormat();
        $this->db = new \application\data\dbClass();
    }


    public function getCartStr()
    {
        try {
            session_start();

            $products = json_decode($_SESSION['products'], true);
            $productsArticles = json_decode($_SESSION['articles'], true);

            if ($productsArticles != null) {
                $sum = 0;
                foreach ($productsArticles as $id => $product) {
                    $sum += $product['price'] * $product['count'];
                }
            }

            return 'В корзине товаров: ' . (int)(count($productsArticles)) . ' на ' . (int)$sum . ' рублей';

        } catch (Exception $e) {
            echo false;
        }
    }


    public function getManufactorerSlider()
    {
        try {

            $this->db->connect();

            $data = $this->db->readWhere("manufactorer", 'view', true);
            $resultStr = '';

            foreach ($data as $man) {
                $resultStr .= "
                            <li>
                                <a href='/products/manufacturer/{$man['id']}' title='{$man['name']}'><img src='/resources/imagebrowser/logos/{$man['img']}'/></a>
                            </li>
                
                ";
            }

            return $resultStr;

        } catch (Exception $e) {
            echo false;
        }
    }

}
