<?php

namespace application\models;

use Exception;
use application\core\exceptions\Exception404;
use application\data\listForm;

class modelProducts extends \application\core\Model
{
    public $dataFormat = null;
    public $db = null;

    public function __construct()
    {
        $this->dataFormat = new \application\data\dataFormat();
        $this->db = new \application\data\dbProducts();
    }

    public function addNewOtz($data)
    {
        try {

            $this->db->connect();

            $this->db->addOtz($data);

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getProduct($id)
    {
        try {
            $data = array();

            $this->db->connect();

            $result = $this->db->read(array('product', $id[0]));

            if (!$result)
                throw new Exception404();

            $data['category'] = $this->db->readWhere("category", 'id', $result['cat_id']);
            $result['category'] = $data['category'][0]['name'];

            $data['filter_type'] = $this->db->selectTable(array('filter_type'));
            $data['oldFilters'] = $this->db->readWhere("filter", 'id_product', $id[0]);

            $data['catFilters'] = $this->db->readWhere('cat_filter_type', 'id_category', $result['cat_id']);

            $data['content'] = $result;

            $data['otz'] = $this->db->getOtz($id[0]);

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getShoppinCart(array $products)
    {
        try {
            $data = array();

            $total = 0;

            $this->db->connect();

            foreach ($products as $art => $product) {
                $data['content'][$art] = $this->db->getShoppingProduct($product['id']);
                $data['content'][$art]['article'] = ($product['article'] != '' ? $product['article'] : $art);
                $data['content'][$art]['count'] = $product['count'];
                if ($data['content'][$art]['act'] == 0)
                    $data['content'][$art]['totalprice'] = $product['count'] * $data['content'][$art]['price'];
                else
                    $data['content'][$art]['totalprice'] = $product['count'] * $data['content'][$art]['act'];

                $total += $data['content'][$art]['totalprice'];
            }

            $data['content']['total'] = $total;

            return $data;


        } catch (Exception $e) {
            throw $e;
        }
    }


    public function newOrder(array $products, array $post)
    {
        try {
            $this->db->connect();


            $data['address'] = "<b>Индекс:</b> {$post['index']} <br>
                        <b>Край \ область:</b> {$post['obl']} <br>
                        <b>Город \ населенный пункт:</b> {$post['town']} <br>
                        <b>Полный адрес:</b> {$post['adress']} <br>";

            $data['text'] = json_encode($products, true);

            $this->db->addOrder($data, $post);

            session_destroy();

            return true;

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function updateShoppingCart(array $post, array $products)
    {

        foreach ($post as $key => $value) {
            if ($value > 0) {
                $products[$key]['count'] = ((int)$value) . '';
            } else {
                unset($products[$key]);
            }
        }
        session_start();

        $_SESSION['articles'] = json_encode($products, true);

    }


    private function getDataForCatagory(array $category, $pages)
    {
        try {
            $this->db->connect();

            $data = $this->db->selectProductsTableByKey($category['id'], null, $pages );

            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function productList(array $id, array $post)
    {
        try {

            $data = array();
            parse_str($id[2], $newPost);

            $data['post'] = $newPost;

            if ($_POST != null) {

                $newPost = array();
                foreach ($_POST as $key => $value)
                    if (strpos($key, '_input') === false)
                        $newPost[$key] = $value;

                $id[1] = 1;
                $newPost = $this->dataFormat->deleteSl($newPost);
                $path = http_build_query($newPost);

                header("Location: /products/list/{$id[0]}/{$id[1]}/{$path}");
            } else {

                $this->db->connect();

                $data['category'] = $this->db->readWhere('category', 'c_key', $id[0]);
                $data['category'] = $data['category'][0];

                if ($data['category'] == null)
                    throw new Exception404();

                $data['cat_filter_type'] = $this->db->readWhere('cat_filter_type', 'id_category', $data['category']['id']);
                $data['filter_type'] = $this->db->selectTable(array("filter_type"));

                $data['price'] = $this->db->getPriceForCategory($data['category']['id']);

                foreach ($data['cat_filter_type'] as $value) {
                    $data['cat_filters'][$value['id_filter_type']] = $this->db->selectFilters($value['id_filter_type']);
                }

                $path = $id[2];

                $data['pages'] = $this->productListPages($id, $data['category']);
                if ($path != null and is_array($newPost)) {

                    // Выборка товаров по фильтрам-------------

                    $count = 0;
                    $filterProductListFirst = array();  // просто выборка фильтров товаров
                    foreach ($newPost as $key => $value) {
                        if ((int)$key !== 0 and $value !== '') {
                            $count++;
                            $tempResult = $this->db->readWhereWhatArray('filter', 'id_product', array('id_filter_type', 'id_category', 'value'), array($key, $data['category']['id'], $value));
                            foreach ($tempResult as $val)
                                $filterProductListFirst[$key][] = $val['id_product'];
                        }
                    }

                    $filterProductList = array();
                    foreach ($filterProductListFirst as $filterKey => $filterValues) {
                        foreach ($filterValues as $value) {
                            $filterProductList[$value][$filterKey] = 1;
                        }
                    }
                    $resultProductList = array();

                    foreach ($filterProductList as $key => $val) {
                        if (count($val) === $count)
                            $resultProductList[] = $key;
                    }

                    $listForm = new \application\data\listForm($newPost);


                    if (!$listForm->nullData()) {

                        $listForm->filterProducts = $resultProductList;

                       $result = $this->getDataByListForm($listForm, $data['category'], $data['pages']);

                       $data['content'] = $result['content'];
                       $data['pages'] = $result['pages'];

                    } else {
                        $data['content'] = $this->getDataForCatagory($data['category'], $data['pages']);
                    }

                } else {
                    $data['content'] = $this->getDataForCatagory($data['category'], $data['pages']);
                }

                $data['manufactorer'] = $this->db->selectTable(array('manufactorer'));
                $data['manufactorer'] = $this->idKeys($data['manufactorer'], 'id');

                if ($data['content'] != null){
                    foreach ($data['content'] as $product) {
                        $data['pr_filters'][$product['id']] = $this->productGetFilters($product);
                    }
                }

                return $data;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getDataByListForm(listForm $listForm, array $category, array $pages)
    {
        try {

            $this->db->connect();

            $data = array();

            $params = $listForm->getQueryString();

            $count = $this->db->getCountForParams($category['id'], $params);
            $count = (int)$count['COUNT(*)'];

            if ($count !== 0) {
                $data['pages'] = $pages;
                $data['pages']['count'] = $count;

                $data['content'] = $this->db->selectProductsTableByKey($category['id'], $params, $pages);
            }


            return $data;

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function idKeys(array $arr, $id)
    {

        $result = array();

        foreach ($arr as $value) {
            $result[$value[$id]] = $value;
        }

        return $result;

    }

    public function productGetFilters(array $product)
    {
        try {
            $this->db->connect();

            $allFilters = $this->db->readWhere('filter', 'id_product', $product['id']);

            $result = array();
            foreach ($allFilters as $filter) {
                $result[$filter['id_filter_type']][] = $filter;
            }

            return $result;

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function productListPages(array $id, array $category)
    {
        try {
            $pages = array();

            $this->db->connect();

            $val = $this->db->getSettingsCount();
            $val = (int)$val['products_count'];

            $page = (int)$id[1];
            if ($page === 0 and $id[1] != '')
                throw new Exception404();

            if ($id[1] == null || $id[1] == 1)
                $with = 0;
            else {
                $with = $val * ((int)$id[1]) - $val;
            }

            $pages['count'] = $this->db->getListCount($category['id']);
            $pages['count'] = (int)$pages['count']['COUNT(*)'];
            $pages['select'] = ($page == 0) ? 1 : $page;
            $pages['size'] = $val;
            $pages['page'] = '/products/list/' . $id[0] . '/';
            $pages['with'] = $with;
$pages['params'] = $id[2];

            return $pages;

        } catch (Exception $e) {
            throw $e;
        }
    }


}
