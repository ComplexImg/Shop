<?php

namespace application\core;

use \application\core\exceptions\Exception404;
use \Exception;
use \PDOException;


class Main
{
    static function goWork()
    {

        try {

            Route::start();

        } catch (Exception404 $e) {

            Route::ErrorPage404();

        } catch (PDOException $e) {

            self::saveException($e);
            Route::ErrorPage500();
         //   var_dump($e);

        } catch (Exception $e) {

            self::saveException($e);
            Route::ErrorPage500();

           // var_dump($e);
        }
    }

    static function saveException(Exception $e)
    {
        $date = date("c");

        $str = "$date:\nip: {$_SERVER['REMOTE_ADDR']}\npath: {$_SERVER['REQUEST_URI']}\n{$e->__toString()}\n\n";
        file_put_contents('log/log.txt', $str, FILE_APPEND | LOCK_EX);

        $date = date("d-m-y---H-i-s");

        $file = '__' . $date;
        file_put_contents("log/$file", json_encode($e, true), FILE_APPEND | LOCK_EX);
    }

}