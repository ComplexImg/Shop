<?php

namespace application\core;

use \application\core\exceptions\Exception404;
use \Exception;

class Route
{

    static function start()
    {
        try {

            $controller_name = 'main';
            $action_name = 'index';
            $action_id = null;

            $routes = explode('/', $_SERVER['REQUEST_URI']);

            if (!empty($routes[1]))
                $controller_name = $routes[1];

            if (!empty($routes[2]))
                $action_name = $routes[2];

            if (!empty($routes[3]))
                $action_id = $routes[3];

            if (!empty($routes[4]))
                $action_id = $routes[3] . '/' . $routes[4] . '/' . $routes[5];


            $controller_name = strtolower($controller_name);

            $controller_name = 'Controller_' . $controller_name;
            $action_name = 'action_' . $action_name;

            $controller_file = $controller_name;
            $controller_path = '\application\controllers\\' . $controller_file;

            if (class_exists($controller_path))
                $controller = new $controller_path;
            else
                throw new Exception404();

            $action = $action_name;

            if ($controller_name === 'Controller_admin' || $controller_name === 'Controller_errors') {
                if (method_exists($controller, $action)) {
                    $controller->$action($action_id);
                } else {
                    throw new Exception404();
                }
            } else {
                $db = new \application\data\dbClass();
                $db->connect();

                if ($db->workSite()) {
                    if (method_exists($controller, $action)) {
                        $controller->$action($action_id);
                    } else {
                        throw new Exception404();
                    }
                } else {
                    self::notWork();
                }
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    static function ErrorPage404()
    {
        try {
            $controller_path = '\application\controllers\Controller_errors';
            if (class_exists($controller_path))
                $controller = new $controller_path;
            else
                throw new Exception404();

            $action = 'action_404';


            if (method_exists($controller, $action)) {
                $controller->$action();
            } else {
                throw new Exception404();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    static function ErrorPage500()
    {
        try {
            $controller_path = '\application\controllers\Controller_errors';
            if (class_exists($controller_path))
                $controller = new $controller_path;
            else
                throw new Exception404();

            $action = 'action_500';


            if (method_exists($controller, $action)) {
                $controller->$action();
            } else {
                throw new Exception404();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    static function notWork()
    {
        try {

            $controller_path = '\application\controllers\Controller_errors';
            if (class_exists($controller_path))
                $controller = new $controller_path;
            else
                throw new Exception404();

            $action = 'action_not_work';


            if (method_exists($controller, $action)) {
                $controller->$action();
            } else {
                throw new Exception404();
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

}
