<?php

include "../lib/Kendo/Autoload.php";
include "./DataSourceResult.php";



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    header('Content-Type: application/json');

    $request = json_decode(file_get_contents('php://input'));

    $result = new DataSourceResult();

    $type = $_GET['type'];

    $columns = array('NewsId', 'NewsText', 'NewsDate');

    switch($type) {
        case 'create':
            $result = $result->create('News', $columns, $request->models, 'NewsId');
            break;
        case 'read':
            $result = $result->read('News', $columns, $request);
            break;
        case 'update':
            $result = $result->update('News', $columns, $request->models, 'NewsId');
            break;
        case 'destroy':
            $result = $result->destroy('News', $request->models, 'NewsId');
            break;
    }

    echo json_encode($result,JSON_NUMERIC_CHECK);

    exit;
}