<?php

spl_autoload_register(function($class) {
    if (strpos($class, "Kendo\\") === 0) {
        $path = str_replace('Kendo', '', $class);

        $path = __DIR__.str_replace('\\', '/', $path).'.php';


        //$str = "$class:\n    $path\n\n";
        //file_put_contents('log.txt', $str, FILE_APPEND | LOCK_EX);

        require_once $path;
    }
});

?>
